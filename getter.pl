#!/usr/bin/perl

use Modern::Perl;
use WWW::Mechanize;
use WWW::Bugzilla;
use XML::LibXML;
use XML::Simple qw(:strict);
use File::Temp qw/ tempfile tempdir /;
use Git::Repository;
use Getopt::Long;
use Date::Simple ( 'date', 'today' );
use IPC::System::Simple qw(system systemx capture capturex);

my $today = today();
my $repository;
our $user;
our $passwd;

GetOptions(
    'r|repo=s'     => \$repository,
    'u|user=s'     => \$user,
    'p|password=s' => \$passwd,
);

my $mech = WWW::Mechanize->new();

print "Checking out repo\n";

# start from an existing repository
my $r = Git::Repository->new( work_tree => $repository );
my $output = $r->run('fetch');
print $output;
$output = $r->run( 'checkout', 'origin/master' );
print $output;

$mech->get(
'http://bugs.koha-community.org/bugzilla3/buglist.cgi?bug_status=Needs%20Signoff&query_format=advanced&title=Bug%20List&order=changeddate%20ASC%2Cbug_severity%2Cbug_id%20DESC'
);

$mech->submit_form( form_number => 5, );
my $dom = XML::LibXML->load_xml( string => $mech->content );

my $root = $dom->documentElement();

foreach my $node ( $dom->findnodes('/bugzilla/bug') ) {
    my $broken            = 0;
    my ($bug_number_node) = $node->findnodes('./bug_id');
    my $bug_number        = $bug_number_node->to_literal;
    my ($bot_node)        = $node->findnodes('./cf_bot');
    my ($bot_date)        = $node->findnodes('./cf_lastchecked');
    if ( $bot_date && $bot_date->to_literal ) {
        my $datetime = $bot_date->to_literal;
        my @date     = split( / /, $datetime );
        my $date     = Date::Simple->new( $date[0] );
        warn $date;
        warn $today;
        my $diff = $today - $date;
        if ( $diff < 14 ) {
            print "Skipping bug $bug_number, was checked $diff days ago\n";
            next;

        }
    }
    my $control = $bot_node->to_literal;
    if ( $control eq 'Ignore this bug' ) {
        print "Ignoring bug $bug_number, as we have been told to\n";
        next;
    }
    my @depends_node = $node->findnodes('./dependson');
    foreach my $depend (@depends_node) {
        print "$bug_number depends on " . $depend->to_literal . "\n";
        $mech->get(
"http://bugs.koha-community.org/bugzilla3/show_bug.cgi?ctype=xml&id="
              . $depend->to_literal );
        my $bug_dom = XML::LibXML->load_xml( string => $mech->content );
        my ($bug_status) = $bug_dom->findnodes('/bugzilla/bug/bug_status');

        my $status = $bug_status->to_literal;
        if (   $status eq 'Needs Signoff'
            || $status eq 'Signed Off'
            || $status eq 'Passed QA' )
        {

            my $bz = bz( $depend->to_literal );
            my @attachments;
            eval {
                @attachments = $bz->list_attachments( $depend->to_literal );
            };
            if ($@) {
                $bz = bz($bug_number);
                $bz->additional_comments( "This bug depends on "
                      . $depend->to_literal
                      . " which has no valid patches" );
                $bz->{'cf_lastchecked'} = today();
                $bz->commit;
                $broken = 1;
                last;

            }
            else {
                my $result =
                  apply_attachments( \@attachments, $r, $bz, $bug_number,
                    $control, 1 );
                $r->run( 'am', '--abort' );
                $r->run( 'clean', '-d', '-f' );
                $r->run('stash');
                if ( $result eq 'failed' ) {
                    $broken = 1;

                    $bz = bz($bug_number);
                    $bz->additional_comments( "This bug depends on bug"
                          . $depend->to_literal
                          . " which is in status $status but the patches for it do not apply cleanly"
                    );
                    $bz->{'cf_lastchecked'} = today();
                    $bz->commit;
                }
            }
        }
        elsif ($status eq "Patch doesn't apply"
            || $status eq 'In Discussion'
            || $status eq 'Failed QA' )
        {
            my $bz = bz($bug_number);
            $bz->additional_comments( "This bug depends on "
                  . $depend->to_literal
                  . " which is in status $status" );
            $bz->{'cf_lastchecked'} = today();
            $bz->commit;
            $broken = 1;
            last;
        }
        else {
            next;
        }
    }
    if ($broken) {
        next;
    }
    my $bz = bz($bug_number);
    print "Trying bug $bug_number\n";

    my @attachments;
    eval { @attachments = $bz->list_attachments($bug_number); };
    if ($@) {
        next;
    }

    apply_attachments( \@attachments, $r, $bz, $bug_number, $control );
    $r->run( 'am', '--abort' );
    $r->run( 'clean', '-d', '-f' );
    $r->run('stash');

    print "\n\n";
    $output = $r->run('fetch');
    print $output;
    $output = $r->run( 'checkout', 'origin/master' );
    print "\n";
    print $output;
}

sub bz {
    my $bug_number = shift;
    my $bz         = WWW::Bugzilla->new(
        server     => 'bugs.koha-community.org/bugzilla3/',
        email      => $user,
        password   => $passwd,
        bug_number => $bug_number
    );
    return $bz;
}

sub apply_attachments {
    my $attachments = shift;
    my $r           = shift;
    my $bz          = shift;
    my $bug_number  = shift;
    my $control     = shift;
    my $depends     = shift;

    my $patch_count = 0;
    foreach my $attachment (@$attachments) {
        if ( $attachment->{'obsolete'} || !$attachment->{'patch'} ) {
            print "Skipping patch because obsolete or not a patch\n";
            next;
        }

        my $id = $attachment->{'id'};
        my $file = $bz->get_attachment( id => $id );

        my ( $fh, $filename ) = tempfile();
        print $fh $file;

        $output = $r->run( 'am', '-u3', $filename );
        if (   $output =~ /CONFLICT/
            || $output =~ /lacking or useless/
            || $output =~
            /Repository lacks necessary blobs to fall back on 3-way merge./ )
        {
            $r->run( 'am', '--abort' );
            $r->run( 'clean', '-d', '-f' );
            $r->run('stash');
            $bz = WWW::Bugzilla->new(
                server     => 'bugs.koha-community.org/bugzilla3/',
                email      => $user,
                password   => $passwd,
                bug_number => $bug_number,
            );

            $bz->{'cf_lastchecked'} = today();
            $bz->additional_comments($output);
            if ( $control ne 'No Status change' ) {
                $bz->change_status("Patch doesn't apply");
            }
            $bz->commit;
            print $output;
            return 'failed';
        }

        print $output;
        $patch_count++;
    }
    unless ($depends) {
        $bz = WWW::Bugzilla->new(
            server     => 'bugs.koha-community.org/bugzilla3/',
            email      => $user,
            password   => $passwd,
            bug_number => $bug_number,
        );
        $bz->{'cf_lastchecked'} = today();
        my $output;
        eval { $output =
          capture("/home/christopher/git/koha-git-tester/qa.sh 1");
        };
        if ($@){
            print $@;
            print $output;
            exit;
        }
        $bz->additional_comments('Patch applied cleanly\n');
        $bz->additional_comments($output);
        $bz->commit;

    }

}
