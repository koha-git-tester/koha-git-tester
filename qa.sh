#!/bin/sh

export PERL5LIB=/home/christopher/perl5/lib/perl5/x86_64-linux-gnu-thread-multi:/home/christopher/perl5/lib/perl5:/home/christopher/git/qa-test-tools/:/home/christopher/git/koha  
export KOHA_CONF=/etc/koha/sites/persona/koha-conf.xml 

cd /home/christopher/git/koha
/home/christopher/git/qa-test-tools/koha-qa.pl -c $1
cd /home/christopher/git/koha-git-tester

